#!/usr/bin/python3

import setuptools

setuptools.setup(
    name='yoseeg',
    version="0.1alpha",
    description='SEE node generator',
    author='EP-ESE-ME, CERN',
    packages=['yoseeg'],
    include_package_data=True,
    scripts=['yoseeg/lib2yoseeg.sh'],
    entry_points={
        'console_scripts': ['yoseeg = yoseeg.yoseeg:main',
                            'lib2yoseeg = yoseeg.yoseeg:lib2yoseeg']
    }
)
