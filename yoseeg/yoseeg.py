#!/usr/bin/env python
import logging
import argparse
import os
import string
from pyosys import libyosys as ys

set_nodes = []
seu_nodes = []

def process_module(design, module, cell, hierarchy_string):
    global set_nodes
    global seu_nodes
    if len(module.cells_): # we have to go deeper
        for subcell in module.selected_cells():
            submod = design.module(subcell.type)
            subcellname = subcell.name.str()
            logging.debug("Processing submodule: %s" % (subcellname))
            if "[" in subcellname:
                process_module(design, submod, subcell, '.'.join([hierarchy_string, subcellname + " "]))
            else:
                process_module(design, submod, subcell, '.'.join([hierarchy_string, subcellname[1:]]))
    else:
        logging.debug("Hier: %s, Mod: %s" % (hierarchy_string, module.name.str()))
        for modport in module.ports:
            portwire = module.wire(modport)

            if portwire.port_output:
                if portwire.width == 1:
                    set_nodes.append('.'.join([hierarchy_string, modport.str()[1:]]))
                else:
                    if portwire.upto:
                        maxidx = portwire.start_offset - portwire.width
                        minidx = portwire.start_offset
                    else:
                        minidx = portwire.start_offset
                        maxidx = portwire.start_offset + portwire.width
                    for i in range(minidx, maxidx):
                        set_nodes.append('.'.join([hierarchy_string, modport.str()[1:] + "[%d]" % i]))

        for param in module.avail_parameters:
            strparam = param.str()
            if any(seu_str in strparam for seu_str in ["seu_set", "seu_reset"]):
                seu_sig = strparam[strparam.rfind("_")+1:]
                seu_nodes.append('.'.join([hierarchy_string, seu_sig]))
                
# this is tmrg.toolset.generateFromTemplate
def generateFromTemplate(outFname, templateFname, values):
    f = open(templateFname, "r")
    temp = f.read()
    f.close()
    values_subst = {}
    values_subst["set_max_net"] = values["set_max_net"]
    values_subst["seu_max_net"] = values["seu_max_net"]
    temp_subst = string.Template(temp).substitute(values_subst)

    f = open(outFname, "w")
    for line in temp_subst.split("\n"):
        if line.startswith("<"):
            tpl_key = line[1:-1]
            f.write(values[tpl_key])
        else:
            f.write(line)
            f.write('\n')
    f.close()


def main():
    global seu_nodes
    global set_nodes

    parser = argparse.ArgumentParser(description="Yosys SEE generator")
    parser.add_argument("-e", "--exclude", dest="exclude", default="", help="Exclude nets from output file")
    parser.add_argument("-o", "--output-file", dest="ofile", default="see.v", help="Output file name")
    parser.add_argument("-l", "--library", dest="libs", default=[], action="append", help="Library file names")
    parser.add_argument("-t", "--top-level", dest="top", help="Top level module name")
    parser.add_argument("-v", "--verbose", dest="verbose", action="store_true", help="Verbose debugging output")
    parser.add_argument("-d", "--debug", dest="debug", action='store_true', default=False, help="Write debug output to set_nodes.txt and seu_nodes.txt")
    parser.add_argument("files", nargs="*", help="Files to be read")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(format='[%(levelname)-7s] %(message)s', level=logging.DEBUG)
    else:
        logging.basicConfig(format='[%(levelname)-7s] %(message)s', level=logging.INFO)
    if not args.files:
        raise ValueError("No input files given.")

    if not args.top:
        args.top = os.path.splitext(os.path.basename(args.files[0]))[0]

    args.files.extend(args.libs)

    logging.info("[1/5] Reading Netlist and Libraries")
    design = ys.Design()
    ys.run_pass("read_verilog -nomem2reg -noopt %s" % " ".join(args.files), design)
    ys.run_pass("hierarchy -top %s" % args.top, design)

    logging.info("[2/5] Analyzing Netlist Structure")
    top = design.top_module()
    process_module(design, top, None, "DUT")

    logging.info("[3/5] Building SET node list")
    if args.debug:
        with open("set_nodes.txt", "w") as fp:
            for line in set_nodes:
                fp.write(line + "\n")
        
        with open("seu_nodes.txt", "w") as fp:
            for line in seu_nodes:
                fp.write(line + "\n")

    values = {}
    values['see_full_list'] = []
    values['set_display_net'] = []
    values['set_force_net'] = []
    values['set_release_net'] = []
    for wireid, net in enumerate(set_nodes):
        values['set_force_net'].append("    %4d : force %s = ~%s;\n" % (wireid, net, net))
        values['set_release_net'].append("    %4d : release %s;\n" % (wireid, net))
        values['set_display_net'].append('   %4d : $display("%s");\n' % (wireid, net))
        values['see_full_list'].append("// %4d : %s\n" % (wireid, net))

    values['set_max_net'] = "%d" % len(set_nodes)
    values['set_force_net'] = ''.join(values['set_force_net']).rstrip()
    values['set_release_net'] = ''.join(values['set_release_net']).rstrip()
    values['set_display_net'] = ''.join(values['set_display_net']).rstrip()

    if len(values['set_force_net']) == 0:
        logging.warning("No SET wires!")
        values['set_force_net'] = "    // No SET wires found !\n"
        values['set_release_net'] = "    // No SET wires found !\n"
        values['set_display_net'] = "    // No SET wires found !\n"
    else:
        values['set_force_net'] = "    case (wireid)\n" + values['set_force_net'] + "\n    endcase\n"
        values['set_release_net'] = "    case (wireid)\n" + values['set_release_net'] + "\n    endcase\n"
        values['set_display_net'] = "    case (wireid)\n" + values['set_display_net'] + "\n    endcase\n"

    logging.info("[4/5] Building SEU node list")
    values['seu_display_net'] = []
    values['seu_force_net'] = []
    values['seu_release_net'] = []
    for wireid, net in enumerate(seu_nodes):
        values['seu_force_net'].append("    %4d : force %s = ~%s;\n" % (wireid, net, net))
        values['seu_release_net'].append("    %4d : release %s;\n" % (wireid, net))
        values['seu_display_net'].append('   %4d : $display("%s");\n' % (wireid, net))
        values['see_full_list'].append("// %4d : %s\n" % (len(set_nodes)+wireid, net))

    values['seu_max_net'] = "%d" % len(seu_nodes)
    values['seu_force_net'] = ''.join(values['seu_force_net']).rstrip()
    values['seu_release_net'] = ''.join(values['seu_release_net']).rstrip()
    values['seu_display_net'] = ''.join(values['seu_display_net']).rstrip()

    if len(values['seu_force_net']) == 0:
        logging.warning("No SEU wires!")
        values['seu_force_net'] = "    // No SEU wires found !\n"
        values['seu_release_net'] = "    // No SEU wires found !\n"
        values['seu_display_net'] = "    // No SEU wires found !\n"
    else:
        values['seu_force_net'] = "    case (wireid)\n" + values['seu_force_net'] + "\n    endcase\n"
        values['seu_release_net'] = "    case (wireid)\n" + values['seu_release_net'] + "\n    endcase\n"
        values['seu_display_net'] = "    case (wireid)\n" + values['seu_display_net'] + "\n    endcase\n"

    values['see_full_list'].append("\n")
    values['see_full_list'] = ''.join(values['see_full_list'])

    logging.info("[5/5] Generating Output File")
    generateFromTemplate(args.ofile, os.path.join(os.path.dirname(os.path.realpath(__file__)), "seeg.tpl"), values)

def lib2yoseeg():
    parser = argparse.ArgumentParser(description="Yosys SEE generator")
    parser.add_argument("files", nargs="*", help="Files to be read")
    args = parser.parse_args()

    os.system(os.path.join(os.path.dirname(os.path.realpath(__file__)), "lib2yoseeg.sh") + " %s" % " ".join(args.files))


if __name__ == "__main__":
    main()
