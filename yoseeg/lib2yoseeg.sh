#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    exit
fi

# This should really use verilog attributes
#sed '/tmrg/ s/\/\/ tmrg \(.*\) \(.*\)/(* \1 = "\2" *)/' $1 > $2
# But as we can't access those in pyosys, we workaround :)
sed '/tmrg/ s/\/\/ tmrg \(\S*\)\s*\(\S*\)/parameter \1_\2 = 0;/' $1 > $2
